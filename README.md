## Descripción

Esta es mi soluion propuesta al [Desafio Back-end](https://github.com/newcombin/devskillsback) por parte de [NewCombin](https://newcombin.comk).

La solución esta basada en Node.js con un framework TypeScript [Nest](https://github.com/nestjs/nest).

En la parte de la base de datos se utilizo [SQLite](https://www.sqlite.org/index.html) con el ORM [Sequelize](https://sequelize.org)

Para la documentación del API se utilizo [Swagger](https://swagger.io)

## Instalación

[Instalar NestJS](https://docs.nestjs.com)

Despues de tener instalado el cli de NestJS instalar todos los paquetes utilizadas en el proyecto:

```bash
$ npm install
```
**_NOTE:_**  Se debe de crear el archivo con las variables de entorno(.env). Ver archivo env.sample.

## Correr la app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Swagger - Documentación API

Por defecto se manejo la ruta /help para mostrar la documentación referente al API.
ejemplo: http://localhost:3000/help

**_NOTE:_**  La ruta por defecto puede ser modificada desde las variables de entorno. PATH_API_HELP

## Endpoints

| Tipo  | Endpoint | Descripcion  |
|---|---|---|
| GET |  /api/v1/payables | Retorna todas las boletas de pago | 
| POST |  /api/v1/payables | Crea una nueva boleta de pago | 
| GET |  /api/v1/payables/unpaid | Retorna todas las boletas inpago y puede filtrar por tipo de boleta | 
| GET |  /api/v1/transactions | Retorna todas las transacciones | 
| POST |  /api/v1/transactions | Crea una transacción| 
| GET |  /api/v1/transactions/summary/{startDate}/{endDate} | Retorna los pagos en un período de fechas, acumulando por día | 

**_NOTE:_**  Para mas información revisar la documentacion del API (/help)

## Mantengamonos en contacto

-   Author - [Alberto Barrera](https://gitlab.com/biitheck)
-   [LinkedIn](www.linkedin.com/in/alberto-barrera-es)

## Licencia

UNLICENSED
