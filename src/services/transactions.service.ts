import { Inject, Injectable } from '@nestjs/common';
import { Transaction } from './../models';
import { PayablesService } from './payables.service';
import { CreateTransaction } from '../models/dtos/create-transaction.dto';
import { PaymentStatus, PaymenyType } from 'src/enums';
import { TransactionSummaryDTO } from '../models/dtos/transaction-summary.dto';
import { Op } from 'sequelize';
import sequelize from 'sequelize';

@Injectable()
export class TransactionsService {
    constructor(
        @Inject('TRANSACTION_REPOSITORY')
        private transactionsRepo: typeof Transaction,
        private payablesSrv: PayablesService,
    ) {}

    async getAll(): Promise<Transaction[]> {
        return this.transactionsRepo.findAll<Transaction>();
    }

    async getSummary(
        startDate: Date,
        endDate: Date,
    ): Promise<TransactionSummaryDTO[]> {
        return await this.transactionsRepo.findAll<any>({
            attributes: [
                'paymentDate',
                [sequelize.fn('count', sequelize.col('id')), 'transactionsNo'],
                [sequelize.fn('sum', sequelize.col('amount')), 'totalAmount'],
            ],
            where: {
                paymentDate: {
                    [Op.between]: [startDate, endDate],
                },
            },
            group: ['paymentDate'],
        });
    }

    async add(payload: CreateTransaction): Promise<Transaction> {
        const payable = await this.payablesSrv.findBySku(payload.sku);

        if (!payable?.id) {
            throw 'Invalid SKU';
        }

        if (payable.status === PaymentStatus.COMPLETE) {
            throw 'the sku is already paid';
        }

        if (payload.paymenyType !== PaymenyType.CASH && !payload.cardNumber) {
            throw 'Invalid Card Number';
        }

        if (payable.amount !== payload.amount) {
            throw 'Invalid Amount';
        }

        const rows = await this.payablesSrv.update(
            { status: PaymentStatus.COMPLETE },
            payable.id,
        );

        if (rows[0] > 0) {
            return this.transactionsRepo.create({
                ...payload,
                payableId: payable.id,
            });
        }

        throw 'Internal Error...';
    }
}
