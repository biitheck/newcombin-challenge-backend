import { Inject, Injectable } from '@nestjs/common';
import { Payable } from './../models';
import { CreatePayable, PayableDTO } from '../models/dtos';
import { TypeService } from '../enums/type-service.enum';
import { PaymentStatus } from '../enums/payment-status.enum';
import { Op } from 'sequelize';

@Injectable()
export class PayablesService {
    constructor(
        @Inject('PAYABLE_REPOSITORY') private payablesRepo: typeof Payable,
    ) {}

    async getAll(): Promise<Payable[]> {
        return this.payablesRepo.findAll<Payable>();
    }

    async getUnpaid(typeService?: TypeService): Promise<Payable[]> {
        let query = {};

        if (typeService) {
            query = { typeService: typeService };
        }

        return this.payablesRepo.findAll<Payable>({
            where: {
                status: {
                    [Op.ne]: PaymentStatus.COMPLETE,
                },
                ...query,
            },
        });
    }

    async add(payload: CreatePayable): Promise<Payable> {
        return this.payablesRepo.create({ ...payload });
    }

    async update(payload: any, id: string): Promise<[affectedCount: number]> {
        return this.payablesRepo.update(
            {
                ...payload,
            },
            {
                where: { id: id },
            },
        );
    }

    async findBySku(sku: string): Promise<Payable> {
        return this.payablesRepo.findOne({ where: { sku: sku } });
    }
}
