import { Module } from '@nestjs/common';
import { AppController } from './controllers';
import { AppService } from './services/app.service';
import { PayablesModule } from './controllers/payables/payables.module';
import { ConfigModule } from '@nestjs/config';
import { TransactionsModule } from './controllers/transactions/transactions.module';

@Module({
    imports: [ConfigModule.forRoot(), PayablesModule, TransactionsModule],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {}
