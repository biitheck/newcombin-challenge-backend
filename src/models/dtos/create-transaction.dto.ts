import { PaymenyType } from '../../enums';
import {
    IsNotEmpty,
    IsString,
    IsPositive,
    IsDate,
    IsEnum,
    IsOptional,
    IsNumber,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CreateTransaction {
    @IsNotEmpty()
    @IsString()
    readonly sku: string;

    @IsNotEmpty()
    @IsEnum(PaymenyType)
    readonly paymenyType: PaymenyType;

    @IsNotEmpty()
    @IsDate()
    @Type(() => Date)
    readonly paymentDate: Date;

    @IsNotEmpty()
    @IsPositive()
    @IsNumber({ maxDecimalPlaces: 2 })
    readonly amount: number;

    @IsOptional()
    @IsNumber()
    cardNumber?: string;
}
