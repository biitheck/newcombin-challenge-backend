import { TypeService } from '../../enums';
import {
    IsDate,
    IsEnum,
    IsNumber,
    IsOptional,
    IsString,
} from 'class-validator';
import { Type } from 'class-transformer';

export class PayableDTO {
    @IsString()
    readonly sku: string;

    @IsEnum(TypeService)
    @IsOptional()
    typeService?: TypeService;

    @IsDate()
    @Type(() => Date)
    readonly dueDate: Date;

    @IsNumber({ maxDecimalPlaces: 2 })
    readonly amount: number;
}
