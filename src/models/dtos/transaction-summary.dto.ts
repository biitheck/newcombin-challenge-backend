import { IsDate, IsNumber } from 'class-validator';
import { Type } from 'class-transformer';

export class TransactionSummaryDTO {
    @IsDate()
    @Type(() => Date)
    readonly paymentDate: Date;

    @IsNumber({ maxDecimalPlaces: 0 })
    readonly totalAmount: number;

    @IsNumber({ maxDecimalPlaces: 0 })
    readonly transactionsNo: number;
}
