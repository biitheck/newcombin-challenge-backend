import { PaymentStatus, TypeService } from '../../enums';
import {
    IsNotEmpty,
    IsString,
    IsDecimal,
    IsPositive,
    IsDate,
    IsEnum,
    IsOptional,
    IsNumber,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CreatePayable {
    @IsNotEmpty()
    @IsString()
    readonly sku: string;

    @IsEnum(TypeService)
    readonly typeService: TypeService;

    @IsDate()
    @Type(() => Date)
    readonly dueDate: Date;

    @IsNotEmpty()
    @IsPositive()
    @IsNumber({ maxDecimalPlaces: 2 })
    readonly amount: number;

    @IsEnum(PaymentStatus)
    @IsOptional()
    readonly status?: PaymentStatus;

    @IsString()
    readonly description?: string;
}
