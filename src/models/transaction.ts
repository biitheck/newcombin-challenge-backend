import { PaymenyType } from './../enums';
import { Payable } from './payable';
import {
    BelongsTo,
    Column,
    DataType,
    ForeignKey,
    Model,
    Table,
} from 'sequelize-typescript';
import { ApiProperty } from '@nestjs/swagger';

@Table({ tableName: 'transctions' })
export class Transaction extends Model {
    @ApiProperty()
    @Column({
        type: DataType.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true,
        defaultValue: DataType.UUIDV4,
    })
    id: string;

    @ApiProperty()
    @Column({
        type: DataType.UUIDV4,
        allowNull: false,
    })
    @ForeignKey(() => Payable)
    payableId: string;

    @ApiProperty()
    @BelongsTo(() => Payable)
    payable: Payable;

    @ApiProperty()
    @Column({
        type: DataType.ENUM({ values: Object.keys(PaymenyType) }),
        allowNull: false,
    })
    paymenyType: PaymenyType;

    @ApiProperty()
    @Column({
        type: DataType.DATE,
        allowNull: false,
    })
    paymentDate: Date;

    @ApiProperty()
    @Column({
        type: DataType.DECIMAL,
        allowNull: false,
    })
    amount: number;

    @ApiProperty()
    @Column({
        type: DataType.STRING(16),
    })
    cardNumber: string;
}
