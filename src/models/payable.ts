import { PaymentStatus, TypeService } from './../enums';
import { Column, DataType, Model, Table } from 'sequelize-typescript';
import { ApiProperty } from '@nestjs/swagger';

@Table({ tableName: 'payables' })
export class Payable extends Model {
    @ApiProperty()
    @Column({
        type: DataType.UUID,
        primaryKey: true,
        allowNull: false,
        unique: true,
        defaultValue: DataType.UUIDV4,
    })
    id: string;

    @ApiProperty()
    @Column({
        type: DataType.STRING(45),
        allowNull: false,
        unique: true,
    })
    sku: string;

    @ApiProperty()
    @Column({
        type: DataType.ENUM({ values: Object.keys(TypeService) }),
        allowNull: false,
    })
    typeService: TypeService;

    @ApiProperty()
    @Column({
        type: DataType.DATE,
        allowNull: false,
    })
    dueDate: Date;

    @ApiProperty()
    @Column({
        type: DataType.DECIMAL,
        allowNull: false,
    })
    amount: number;

    @ApiProperty()
    @Column({
        type: DataType.ENUM({ values: Object.keys(PaymentStatus) }),
        allowNull: false,
        defaultValue: PaymentStatus.PENDING,
    })
    status: PaymentStatus;

    @ApiProperty()
    @Column
    description: string;
}
