export enum PaymenyType {
    DEBIT_CARD = 'DEBITO',
    CREDIT_CARD = 'CREDITO',
    CASH = 'EFECTIVO',
}
