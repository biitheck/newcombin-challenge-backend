export enum PaymentStatus {
    PENDING = 'PENDIENTE',
    COMPLETE = 'PAGADO',
    OVERDUE = 'ATRASADO',
    CANCELLED = 'CANCELADO',
    REFUNDED = 'RECHAZADO',
}
