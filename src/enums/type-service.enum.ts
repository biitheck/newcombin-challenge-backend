export enum TypeService {
    ELECTRICITY_BILL = 'LUZ',
    WATER_BILL = 'AGUA',
    GAS_BILL = 'GAS',
    INTERNET_BILL = 'INTERNET',
}
