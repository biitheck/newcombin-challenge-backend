import {
    BadRequestException,
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Post,
    Query,
} from '@nestjs/common';
import { PayablesService } from '../../services';
import { Payable } from '../../models/payable';
import { CreatePayable, PayableDTO } from '../../models/dtos';
import { ApiOkResponse, ApiQuery } from '@nestjs/swagger';
import _ from 'lodash';

@Controller('payables')
export class PayablesController {
    constructor(private readonly payableSrv: PayablesService) {}

    @Get()
    @ApiOkResponse({
        description: 'List of the all items',
        type: Payable,
        isArray: true,
    })
    async get(): Promise<Payable[]> {
        return await this.payableSrv.getAll();
    }

    @Get('unpaid')
    @ApiOkResponse({
        description: 'List of the filtered items',
        type: PayableDTO,
        isArray: true,
    })
    @ApiQuery({
        name: 'typeService',
        description: 'Type service',
        required: false,
        type: String,
    })
    async getUnpaid(@Query('typeService') typeService?): Promise<PayableDTO[]> {
        const payables = await this.payableSrv.getUnpaid(typeService);

        const dtos = payables.map((m) => {
            return {
                sku: m.sku,
                typeService: typeService ? undefined : m.typeService,
                dueDate: m.dueDate,
                amount: m.amount,
            } as PayableDTO;
        });

        return dtos;
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @ApiOkResponse({ description: 'The newly created item', type: Payable })
    async create(@Body() payload: CreatePayable) {
        const found = await this.payableSrv.findBySku(payload.sku);

        if (found) {
            throw new BadRequestException('Sku is already exist');
        }

        return await this.payableSrv.add(payload);
    }
}
