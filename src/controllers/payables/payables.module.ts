import { Module } from '@nestjs/common';
import { PayablesController } from './payables.controller';
import { DatabaseModule } from './../../database/database.module';
import { PayablesService } from './../../services';
import { payableProviders } from './../../providers';

@Module({
    imports: [DatabaseModule],
    controllers: [PayablesController],
    providers: [PayablesService, ...payableProviders],
})
export class PayablesModule {}
