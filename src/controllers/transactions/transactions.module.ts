import { Module } from '@nestjs/common';
import { TransactionsController } from './transactions.controller';
import { DatabaseModule } from './../../database/database.module';
import { PayablesService, TransactionsService } from './../../services';
import { payableProviders, transactionProviders } from './../../providers';

@Module({
    imports: [DatabaseModule],
    controllers: [TransactionsController],
    providers: [
        TransactionsService,
        ...transactionProviders,
        PayablesService,
        ...payableProviders,
    ],
})
export class TransactionsModule {}
