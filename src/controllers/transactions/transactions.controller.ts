import {
    BadRequestException,
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Post,
} from '@nestjs/common';
import { TransactionsService } from '../../services';
import { Transaction } from '../../models';
import { CreateTransaction } from '../../models/dtos';
import { ApiOkResponse, ApiQuery } from '@nestjs/swagger';
import { TransactionSummaryDTO } from '../../models/dtos/transaction-summary.dto';

@Controller('transactions')
export class TransactionsController {
    constructor(private readonly transactionSrv: TransactionsService) {}

    @Get()
    @ApiOkResponse({
        description: 'List of the all items',
        type: Transaction,
        isArray: true,
    })
    async get(): Promise<Transaction[]> {
        return await this.transactionSrv.getAll();
    }

    @Get('summary/:starDate/:endDate')
    @ApiOkResponse({
        description: 'List of the filtered items by date range',
        type: TransactionSummaryDTO,
        isArray: true,
    })
    async getSummary(
        @Param('starDate') starDate: Date,
        @Param('endDate') endDate: Date,
    ): Promise<TransactionSummaryDTO[]> {
        return await this.transactionSrv.getSummary(starDate, endDate);
    }

    @Post()
    @HttpCode(HttpStatus.CREATED)
    @ApiOkResponse({
        description: 'The newly created item',
        type: Transaction,
    })
    async create(@Body() payload: CreateTransaction) {
        try {
            return await this.transactionSrv.add(payload);
        } catch (error) {
            console.log(error);
            throw new BadRequestException(error);
        }
    }
}
