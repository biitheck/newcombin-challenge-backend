import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { getFromContainer, MetadataStorage } from 'class-validator';
import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
import { AppModule } from './app.module';

async function bootstrap() {
    const helpPath = process.env.PATH_API_HELP || 'help';

    const app = await NestFactory.create(AppModule);

    app.setGlobalPrefix('api/v1', { exclude: [helpPath, '/'] });

    app.useGlobalPipes(
        new ValidationPipe({
            transform: true,
            whitelist: true, // Ignore missing fields on the DTO.
            forbidNonWhitelisted: true, // Throw error if there is forbidden data.
            disableErrorMessages: false, // Disable error messages (production).
        }),
    );

    const config = new DocumentBuilder()
        .setTitle('NewCombin Desafio Back-end')
        .setDescription(
            'En este desafío, creará una API REST de una versión súper simplificada de un proveedor de servicios de pago de impuestos.',
        )
        .setVersion('0.0.01')
        .addTag('NewCombin', 'Back-end')
        .build();

    const document = SwaggerModule.createDocument(app, config);

    // Creating all the swagger schemas based on the class-validator decorators
    const metadatas = (getFromContainer(MetadataStorage) as any)
        .validationMetadatas;
    const schemas = validationMetadatasToSchemas(metadatas);
    document.components.schemas = Object.assign(
        {},
        document.components.schemas || {},
        schemas,
    );

    SwaggerModule.setup(helpPath, app, document);

    await app.listen(process.env.PORT || 3000);
}
bootstrap();
