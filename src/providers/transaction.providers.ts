import { Transaction } from './../models/transaction';

export const transactionProviders = [
    {
        provide: 'TRANSACTION_REPOSITORY',
        useValue: Transaction,
    },
];
