import { Sequelize } from 'sequelize-typescript';
import { Payable, Transaction } from '../models';

export const databaseProviders = [
    {
        provide: 'SEQUELIZE',
        useFactory: async () => {
            const sequelize = new Sequelize({
                dialect: 'sqlite',
                storage: process.env.DB_PATH,
            }); // :memory:
            sequelize.addModels([Payable, Transaction]);
            await sequelize.sync();
            return sequelize;
        },
    },
];
