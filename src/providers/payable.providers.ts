import { Payable } from './../models/payable';

export const payableProviders = [
    {
        provide: 'PAYABLE_REPOSITORY',
        useValue: Payable,
    },
];
